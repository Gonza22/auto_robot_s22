//Pines para los encoders
int encoder_Izq = 3;
int encoder_Der = 2;

//Contadores para los encoders
volatile int cont_Izq = 0;
volatile int cont_Der = 0;

#define N 20   //Cantidad de ranuras del disco

void fun_encoderIzq();    //Funcion encoder izquierdo
void fun_encoderDer();    //Funcion encoder derecho

void setup() 
{
  Serial.begin(9600);
  pinMode(encoder_Izq, INPUT);
  pinMode(encoder_Der, INPUT);

  attachInterrupt(digitalPinToInterrupt(encoder_Izq), fun_encoderIzq, RISING);
  attachInterrupt(digitalPinToInterrupt(encoder_Der), fun_encoderDer, RISING);
}

void loop() 
{
  long tiempo_act = millis();
  long tiempo_ant = 0;
  int periodo = 1000;

  //Muestro las RPS cada un intervalo de 1S
  if((tiempo_ant + periodo) > tiempo_act)
  {
    Serial.println(cont_Izq / N);    //Cantidad de pulsos, dividido la cantidad de perforaciones del disco
    Serial.print("RPS");

    Serial.println(cont_Der / N);    //Cantidad de pulsos, dividido la cantidad de perforaciones del disco
    Serial.print("RPS");

    //Reinicio variables
    tiempo_ant = tiempo_act;
    cont_Izq = 0;
    cont_Der = 0;
  }
  
}

//Funcion encoder izquierdo
void fun_encoderIzq()
{
  cont_Izq ++;
}

//Funcion encoder derecho
void fun_encoderDer()
{
  cont_Der ++;
}
